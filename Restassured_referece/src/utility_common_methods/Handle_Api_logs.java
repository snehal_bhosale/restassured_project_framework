package utility_common_methods;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_Api_logs {
	


public static void evidence_creator(File dirName,String FileName,String endpoint,String requestBody,String responseBody) throws IOException {
		
		File newFile = new File (dirName + "\\" +FileName + ". Txt");
		System.out.println("To save request & response body we've created new file name :" + newFile.getName());
		FileWriter dataWriter = new FileWriter(newFile);
		dataWriter.write("Request body is :" + requestBody + "\n\n");
		dataWriter.write("End-point is :" + endpoint + "\n\n");
		dataWriter.write("Response body is :" + responseBody);
		dataWriter.close();
		
		
	}


public static void evidence_creator(File dirName,String FileName,String get_endpoint,String get_responseBody) throws IOException {
	
	File newFile = new File (dirName + "\\" +FileName + ". Txt");
	System.out.println("To save request & response body we've created new file name :" + newFile.getName());
	FileWriter dataWriter = new FileWriter(newFile);
	dataWriter.write("End-point is :" + get_endpoint + "\n\n");
	dataWriter.write("Response body is :" + get_responseBody);
	dataWriter.close();
	
}	
}
