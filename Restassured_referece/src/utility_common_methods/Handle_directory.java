package utility_common_methods;

import java.io.File;

public class Handle_directory {
	
public static File create_log_directory( ) {
	//fetch the current project directory
		String project_dir = System.getProperty("user.dir");
		System.out.println("the current project directory path is : "+project_dir);
		File directory=new File(project_dir + "\\API_logs\\");

		if(directory.exists()) {
		directory.delete();
		System.out.println(directory + ": deleted");
		directory.mkdir();
		System.out.println(directory + ": created");
		}else
		 {
			directory.mkdir();
			System.out.println(directory + ": created");
		}
	
		return directory;
	}
	}
