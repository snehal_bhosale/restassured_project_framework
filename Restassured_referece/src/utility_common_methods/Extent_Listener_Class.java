package utility_common_methods;



	import org.testng.ITestContext;
	import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
	import com.aventstack.extentreports.ExtentTest;
	import com.aventstack.extentreports.Status;
	import com.aventstack.extentreports.markuputils.ExtentColor;
	import com.aventstack.extentreports.markuputils.MarkupHelper;
	import com.aventstack.extentreports.reporter.ExtentSparkReporter;
	import com.aventstack.extentreports.reporter.configuration.Theme;

	public class Extent_Listener_Class implements ITestListener {

		ExtentSparkReporter sparkReporter;
		ExtentReports extentReport;
		ExtentTest test;

		public void reportConfiguration() {
			sparkReporter = new ExtentSparkReporter(".\\extent-report\\report.html");
			extentReport = new ExtentReports();

			extentReport.attachReporter(sparkReporter);

			// Adding system/environment information to reports
			extentReport.setSystemInfo("OS", "Windows 10");
			extentReport.setSystemInfo("users", "snehal");

			// Configurations for changing look & feel of report
			sparkReporter.config().setDocumentTitle("Rest Assured Extent Listener Report");
			sparkReporter.config().setReportName("This is my first Extent-Report");
			sparkReporter.config().setTheme(Theme.DARK);

		}

		// This method will get invoked before start of test case execution.(Same as
		// @BeforeClass Annotation i.e. invokes only once)
		public void onStart(ITestContext result) {
			reportConfiguration();
			System.out.println("On Start method invoked...");
		}

		// This method will get invoked before start of test case execution
		public void onFinish(ITestContext result) {
			System.out.println("On Finished method invoked...");
			extentReport.flush();
		}

		// This method will get invoked whether any of the test case fails
		public void onTestFailure(ITestResult result) {
			System.out.println("Name of test method failed : " + result.getName());
			test = extentReport.createTest(result.getName());
			test.log(Status.FAIL, MarkupHelper.createLabel("Name of the Skipped test case is : " + result.getName(), ExtentColor.RED));
		}

		// This method will get invoked whenever any of the test case is skipped
		public void onTestSkipped(ITestResult result) {
			System.out.println("Name of test method skipped : " + result.getName());
			test = extentReport.createTest(result.getName());
			test.log(Status.SKIP, MarkupHelper.createLabel("Name of the Skipped test case is : " + result.getName(), ExtentColor.YELLOW));
		}

		// This method will get invoked on execution of each test case.(Same as
		// @BeforeTest Annotation i.e 4 times in our project)
		public void onTestStart(ITestResult result) {
			System.out.println("Name of test method started : " + result.getName());
		}

		// This method will get invoked whenever any of the test case pass
		public void onTestSuccess(ITestResult result) {
			System.out.println("Name of test method executed successfully : " + result.getName());
			test = extentReport.createTest(result.getName());
			test.log(Status.PASS, MarkupHelper.createLabel("Name of the Passed test case is : " + result.getName(), ExtentColor.GREEN));
		}

		public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
			// No implementation of this method, as it is not being used in project
		}

	}

