package Api_common_method;

import static io.restassured.RestAssured.given;



public class common_method_handle_api {
	
	public static int post_staticCode (String requestBody,String endpoint) {
		
			int statusCode=given().header("Content-Type","application/json").body(requestBody)
		.when().post(endpoint)
		.then().extract().statusCode ();
		return statusCode;
	}
	
	public static String post_responsBody(String requestBody,String endpoint) {
		
			String responsBody=given().header("Content-Type","application/json").body(requestBody)
				.when().post(endpoint)
				.then().extract().response().asString();	
				return responsBody;
			}
///////////////////////////// put_satuscode///////////////////////////////////////////////////////////////////////////////////	
public static int put_statuscode (String requestbody,String endpoint)
{
int statuscode=given().header("Content-Type","application/json").body(requestbody).
when().put(endpoint).then().extract().statusCode();
return statuscode;
}

public static String put_responsebody (String requestbody,String endpoint)
{
	String responsebody=given().header("Content-Type","application/json").body(requestbody).
when().put(endpoint).then().extract().response().asString();
	return responsebody;
}
	///////////////////////////////////////////////patch_statuscode//////////////////////////////////////////////////////////////////////
	public static int patch_statuscode (Object patch_requestBody, String patch_endpoint)
	{
		int statuscode=given().header("Content-Type", "application/json").body(patch_requestBody).
				when().patch(patch_endpoint).then().extract().statusCode();
		return statuscode;
	}
	
	public static String patch_resposebody (String requestbody,String endpoint)
	{
		String responsebody=given().header("Content-Type","application/json").body(requestbody).
				when().patch(endpoint).then().extract().response().asString();
		return responsebody;
	}
	
	///////////////////////////////////get_statuscode//////////////////////////////////////////////////////////////////////////////
	public static int get_statuscode (String endpoint)
	{
		int statuscode=given().header("Content-Type", "application/json").
				when().get(endpoint).then().extract().statusCode();
		return statuscode;
	}
	
	public static String get_responsebody (String endpoint)
	{
		String responsebody=given().header("Content-Type", "application/json").
				when().get(endpoint).then().extract().response().asString();
		return responsebody;
	}
			
				
}



