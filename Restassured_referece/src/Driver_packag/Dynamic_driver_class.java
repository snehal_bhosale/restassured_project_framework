package Driver_packag;

import java.io.IOException;
import java.util.ArrayList;
import java.lang.reflect.Method;

import utility_common_methods.Excel_data_extractor;

import java.lang.reflect.InvocationTargetException;


public class Dynamic_driver_class {

	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		ArrayList<String> TC_execute = Excel_data_extractor.Excel_data_reader("Book1_Test_data", "Test_cases",
				"Tc_name");

		System.out.println(TC_execute);

		int count = TC_execute.size();
		for (int i = 1; i < count; i++) {
			String TC_Name = TC_execute.get(i);
			System.out.println(TC_Name);

// Call the testcaseclass on runtime by using java.lang.reflect package
			Class<?> Test_class = Class.forName("Test_package." + TC_Name);

// Call the execute method belonging to test class captured in variable TC_name by using java.lang.reflect.method class
			Method execute_method = Test_class.getDeclaredMethod("executor");

// Set the accessibility of method true
			execute_method.setAccessible(true);

// Create the instance of testclass captured in variable name testclassname

			Object instance_of_test_class = Test_class.getDeclaredConstructor().newInstance();

//Execute the test script class fetched in variable Test_class
			execute_method.invoke(instance_of_test_class);

		}
	}

}
