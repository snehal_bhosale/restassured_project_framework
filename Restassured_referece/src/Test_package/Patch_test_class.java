package Test_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Api_common_method.common_method_handle_api;
import Request_repositary.Patch_request_repositary;
import endpoint.Patch_endpoint;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_directory;

public class Patch_test_class extends common_method_handle_api {
	@Test

	public static void executor() throws IOException {
		

		File log_dir = Handle_directory.create_log_directory();
		String Patch_requesBody = Patch_request_repositary.Patch_request_tc3();
		System.out.println(Patch_requesBody);
		String patch_endpoint = Patch_endpoint.Patch_endpoint_tc3();
		System.out.println(patch_endpoint);

		for (int i = 0; i < 5; i++) {

			int statusCode = patch_statuscode(Patch_requesBody, patch_endpoint);
			System.out.println(statusCode);

			if (statusCode == 200) {
				String responseBody = patch_resposebody(Patch_requesBody, patch_endpoint);
				System.out.println(responseBody);
				Patch_test_class.validator(Patch_requesBody, responseBody);
				break;

			} else {
				System.out.println("Epected statuscode not found, hence retrying");
			}

		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(responsebody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime Currentdate = LocalDateTime.now();
		String expecteddate = Currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_date = jsp_res.getString("updatedAt");
		res_date = res_date.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, expecteddate);

	}

}
