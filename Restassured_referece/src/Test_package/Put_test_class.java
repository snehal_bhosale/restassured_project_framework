package Test_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Api_common_method.common_method_handle_api;
import Request_repositary.Put_request_repositary;
import endpoint.Put_endpoint;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_Api_logs;
import utility_common_methods.Handle_directory;

public class Put_test_class extends common_method_handle_api{
	@Test
	

public static void executor()throws IOException {
	File log_dir = Handle_directory.create_log_directory();

	
	String put_requestbody = Put_request_repositary.Put_reqest_Tc2();
		String put_endpoint = Put_endpoint.Put_endpoint_Tc1();
		
		
	for(int i =0;i < 5; i++) {
		
		int statusCode = put_statuscode( put_requestbody,put_endpoint);
		System.out.println(statusCode);
	
		if(statusCode==200) {
String responseBody = put_responsebody(put_requestbody,put_endpoint);
		System.out.println(responseBody);
		Handle_Api_logs.evidence_creator(log_dir,"put_test_class",put_endpoint,put_requestbody,responseBody);
		Put_test_class.validator(put_requestbody, responseBody);
		break;
		
		} else {
			System.out.println("Epected statuscode not found, hence retrying");
	}

}
}
	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req=new JsonPath(responsebody);	
		String req_name=jsp_req.getString("name");
		String req_job=jsp_req.getString("job");
		LocalDateTime Currentdate=LocalDateTime.now();
		String expecteddate=Currentdate.toString().substring(0,11);
		
		JsonPath jsp_res=new JsonPath(responsebody);
		String res_name=jsp_res.getString("name");
		String res_job=jsp_res.getString("job");
		String res_date=jsp_res.getString("updatedAt");
		res_date=res_date.substring(0,11);
		
		Assert.assertEquals(res_name,req_name);
		Assert.assertEquals(res_job,req_job);
		Assert.assertEquals(res_date,expecteddate);
		
	}
		
	}	
	