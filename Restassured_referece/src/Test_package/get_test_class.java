package Test_package;


	import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
	import org.json.JSONObject;
	import org.testng.Assert;
import org.testng.annotations.Test;

import Api_common_method.common_method_handle_api;
import io.restassured.RestAssured;
	import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_Api_logs;
import utility_common_methods.Handle_directory;

	public class get_test_class extends common_method_handle_api {
	@Test

public static void executor()throws IOException {
	File log_dir = Handle_directory.create_log_directory();
	String get_endpoint=endpoint.get_endpoint.get_endpoint_tc3();
	
	
			for (int i = 0; i < 5; i++) {
				int get_statusCode = get_statuscode(get_endpoint);
				System.out.println(get_statusCode);
				if (get_statusCode == 200) {
					String get_responseBody = get_responsebody(get_endpoint);
					System.out.println(get_responseBody);
					Handle_Api_logs.evidence_creator(log_dir, "get_test_class", get_endpoint, get_responseBody);
					get_test_class.validator(get_responseBody);
					break;
				} else {
					System.out.println("expected statuscode is not found hence retrying");
				}

			}
		}
		



public static void validator(String get_responseBody) {
			int expected_id[] = { 7, 8, 9, 10, 11, 12 };
			String expected_firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
			String expected_lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
			String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
					"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
			

			
			JSONObject array_res = new JSONObject(get_responseBody);
			JSONArray dataarray = array_res.getJSONArray("data");
			
			int count = dataarray.length();
			
			for (int i = 0; i < count; i++) {
				
				int res_id = dataarray.getJSONObject(i).getInt("id");
				String res_firstname = dataarray.getJSONObject(i).getString("first_name");
				String res_lastname = dataarray.getJSONObject(i).getString("last_name");
				String res_email = dataarray.getJSONObject(i).getString("email");
				
				int exp_id = expected_id[i];
				String exp_firstname = expected_firstname[i];
				String exp_lastname = expected_lastname[i];
				String exp_emial = expected_email[i];
			
				Assert.assertEquals(res_id,exp_id);
				Assert.assertEquals(res_firstname, exp_firstname);
				Assert.assertEquals(res_lastname, exp_lastname);
				Assert.assertEquals(res_email, exp_emial);

			}
		}
	}
