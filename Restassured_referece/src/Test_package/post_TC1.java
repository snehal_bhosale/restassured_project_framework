package Test_package;

	import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Api_common_method.common_method_handle_api;
import Request_repositary.Post_request_repositary;
import endpoint.Post_endpoint;
import io.restassured.path.json.JsonPath;
import utility_common_methods.Handle_Api_logs;
import utility_common_methods.Handle_directory;
	

	public class post_TC1 extends common_method_handle_api {
		@Test

		public static void executor() throws IOException {
			File log_dir = Handle_directory.create_log_directory();
			String requestBody=Post_request_repositary.Post_request_Tc1();
			String Endpoint=Post_endpoint.Post_endpoint_tc1();
			for (int i = 0; i < 5; i++) {
				int statusCode = post_staticCode(requestBody, Endpoint);
				System.out.println(statusCode);
				if (statusCode == 201) {
					String responseBody = post_responsBody(requestBody, Endpoint);
					System.out.println(responseBody);
					
					String post_Endpoint;
					Handle_Api_logs.evidence_creator(log_dir,"post_TC1",Endpoint,requestBody,responseBody);
					post_TC1.validator( requestBody,  responseBody);
					break;
				} else {
					System.out.println("expected statuscode is not found hence retrying");
				}
			}
		}

		

		public static void validator(String requestbody, String responsebody) {
			JsonPath jsp_req = new JsonPath(requestbody);
			String req_name = jsp_req.getString("name");
			String req_job = jsp_req.getString("job");
			LocalDateTime currentdate = LocalDateTime.now();
			String expecteddate = currentdate.toString().substring(0, 11);
			JsonPath jsp_res = new JsonPath(responsebody);
			String res_name = jsp_res.getString("name");
			String res_job = jsp_res.getString("job");
			String res_id = jsp_res.getString("id");
			String res_createdAt = jsp_res.getString("createdAt");
			res_createdAt = res_createdAt.substring(0, 11);
			Assert.assertEquals(res_name, req_name);
			Assert.assertEquals(res_job, req_job);
			Assert.assertNotNull(res_id);
			Assert.assertEquals(res_createdAt, expecteddate);
		}
	}

