import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Post_reference {

	public static void main(String[] args) {
	
		//step 1 Declare the base URL
		RestAssured.baseURI="https://reqres.in/";
		//step 2 configure the request parameters and trigger the api
		String responsebody=given().header("Content-Type","application/json").log().all()
				.body("\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"zion resident\"\r\n"
						+ "}")
				.when().post("api/users")
				.then().log().all().extract().response().asString();
		
		System.out.println("Validation"+responsebody);
		//step 3 : create an object of json path to parse the responsebody and responsebody
		JsonPath jsp_req=new JsonPath(responsebody);	
		String req_name=jsp_req.getString("name");
		String req_job=jsp_req.getString("job");
		LocalDateTime Currentdate=LocalDateTime.now();
		String expecteddate=Currentdate.toString().substring(0,11);
		
		JsonPath jsp_res=new JsonPath(responsebody);
		String res_name=jsp_res.getString("name");
		String res_job=jsp_res.getString("job");
		String res_date=jsp_res.getString("updatedAt");
		res_date=res_date.substring(0,11);
		
		//step 4 : Validate Responsebody
		Assert.assertEquals(res_name,req_name);
		Assert.assertEquals(res_job,req_job);
		Assert.assertEquals(res_date,expecteddate);
		
		
		
	}

}
